import java.util.Arrays;

public class Subarray {

    private double[] originalArray;
    private int from;
    private int to;

    public Subarray(double[] originalArray, int from, int to) {
        this.originalArray = originalArray;
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public double[] getOriginalArray() {
        return originalArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subarray subarray = (Subarray) o;

        if (from != subarray.from) return false;
        if (to != subarray.to) return false;
        if (!Arrays.equals(originalArray, subarray.originalArray)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = originalArray != null ? Arrays.hashCode(originalArray) : 0;
        result = 31 * result + from;
        result = 31 * result + to;
        return result;
    }

    @Override
    public String toString() {
        return "Subarray{" +
                "originalArray=" + Arrays.toString(originalArray) +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}
