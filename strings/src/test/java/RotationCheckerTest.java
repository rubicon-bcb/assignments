import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RotationCheckerTest {

    private RotationChecker rotationChecker = new RotationChecker();

    @Test
    public void testIsRotation() throws Exception {
        assertTrue(rotationChecker.isRotation("abc", "bca"));
        assertTrue(rotationChecker.isRotation("xxxxxxy", "xxxyxxx"));
    }

}