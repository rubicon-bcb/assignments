/**
 * @author sbelov
 */
public interface Predicate<T> {
    /**
     * Simple filter
     * @param value
     * @return true, if filter passes; false otherwise
     */
    boolean apply(T value);
}
